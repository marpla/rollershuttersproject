#include "Arduino.h"
#include <Bounce2.h>
#include <EEPROM.h>
//The setup function is called once at startup of the sketch

#define UP 2
#define OFF 0
#define DOWN 1


//	          power-relay  -|   |-  switching relay
static int pinmap[][2] = {
		{ 9, 35 },  //Rol1
		{ 13, 34 }, { 10, 33 }, { 24, 31 }, { 11, 28 }, { 22, 30 }, { 14, 29 },
		{ 23, 32 } };  //Rol8

////	          power-relay  -|   |-  switching relay
//static int pinmap[][2] = { { 23, 32 }, { 14, 29 }, { 22, 30 }, { 11, 28 }, { 24,
//		31 }, { 10, 33 }, { 13, 34 }, { 9, 35 } };
//	                   UP  -|   |-  DOWN
static int inputPinmap[][2] = { { 2, 3 },  //Rol1
		{ 4, 5 }, { 6, 7 }, { 8, 36 }, { 38, 39 }, { 40, 41 }, { 37, 42 }, { 43,
				44 } };  //Rol8

byte state[] = { OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF }; // 00 00 00 00 00 00 00 00

byte shutter = 0;
byte command = 0;
byte newState = 0;

unsigned long buttonLastPressed[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
unsigned long stateLastChanged[] = { 0, 0, 0, 0, 0, 0, 0, 0 };

Bounce * buttons = new Bounce[16];

unsigned long currentMillis;
unsigned long lastAutoholdCheck = 0;

struct configuration {
	unsigned int debounceInterval;
	unsigned int buttonHoldInterval;
	unsigned int clearAutoHoldInterval;

};
configuration config = { 50, 2000, 60000 };

void setup() {
	Serial.begin(9600);

	//EEPROM.put(0, config);

	EEPROM.get(0, config);
	for (int i = 0; i < 8; i++) {
		pinMode(pinmap[i][0], OUTPUT);
		pinMode(pinmap[i][1], OUTPUT);
		digitalWrite(pinmap[i][0], HIGH);
		digitalWrite(pinmap[i][1], HIGH);

		buttons[2 * i].attach(inputPinmap[i][0], INPUT_PULLUP); //setup the bounce instance for the current button
		buttons[2 * i].interval(config.debounceInterval);      // interval in ms

		buttons[2 * i + 1].attach(inputPinmap[i][1], INPUT_PULLUP); //setup the bounce instance for the current button
		buttons[2 * i + 1].interval(config.debounceInterval);  // interval in ms
	}

	// RELAY TEST -------------------------
	for (int i = 0; i < 8; i++) {
		digitalWrite(pinmap[i][1], LOW);
		delay(50);
		digitalWrite(pinmap[i][1], HIGH);

	}
	//------------------------------------

// Add your initialization code here
}

void stateToRelay() {
	for (int i = 0; i < 8; i++) {
		if (state[i] == OFF) {
			digitalWrite(pinmap[i][1], HIGH);
			digitalWrite(pinmap[i][0], HIGH);
		} else if (state[i] == UP) {
			digitalWrite(pinmap[i][1], HIGH);
			digitalWrite(pinmap[i][0], LOW);
		} else if (state[i] == DOWN) {
			digitalWrite(pinmap[i][1], LOW);
			digitalWrite(pinmap[i][0], LOW);
		}
	}
}

bool checkParity(byte input) {
	bool parityBit1 = input & B00100000;
	bool parityBit2 = input & B01000000;
	bool parityBit3 = input & B10000000;

	bool ok = ((input & B00000001) ^ ((input & B00000010) >> 1)) ^ parityBit1;
	//Serial.println(ok);
	ok &= ((((input & B00000100) >> 2)) ^ ((input & B00001000) >> 3))
			^ parityBit2;
	//Serial.println(ok);
	ok &= (((input & B00010000) >> 4)) ^ parityBit3;
	//Serial.println(ok);

	return ok;
}

void handleRequest() {
	byte req = command & B00000111;
	//its a request
	switch (req) {
	case 0:
		Serial.print("shutters_state:");
		for (int i = 0; i < 8; i++) {
			Serial.print(state[i]);
			Serial.print(";");
		}
		Serial.println();
		break;
	case 1:
		Serial.print("switch_state:");
		for (int i = 0; i < 8; i++) {
			Serial.print(buttons[2 * i].read());
			Serial.print(buttons[2 * i + 1].read());
			Serial.print(";");
		}
		Serial.println();
		break;
	case 2:
		Serial.print("{");
		Serial.print("version:");
		Serial.print("\"01.2020v1\""); // month - year - number
		Serial.print(",author:");
		Serial.print("\"Martin Plaas <martin.plaas@marpla.org>\"");
		Serial.print(",debounce:");
		Serial.print(config.debounceInterval);
		Serial.print(",buttonHold:");
		Serial.print(config.buttonHoldInterval);
		Serial.print(",clearAutoHold:");
		Serial.print(config.clearAutoHoldInterval);
		Serial.println("}");
		break;
	case 3: {
		// set debounce
		unsigned int value = Serial.parseInt();
		if (value != 0 && value < 1000) {
			config.debounceInterval = value;
			EEPROM.put(0, config);
			EEPROM.get(0, config);
			Serial.print("debounce: ");
			Serial.println(config.debounceInterval);
		}
	}
		break;
	case 4: {
		// set buttonHold
		unsigned int value = Serial.parseInt();
		if (value != 0 && value < 60000) {
			config.buttonHoldInterval = value;
			EEPROM.put(0, config);
			EEPROM.get(0, config);
			Serial.print("buttonHold: ");
			Serial.println(config.buttonHoldInterval);
		}
	}
		break;
	case 5: {
		// set clear auto hold
		unsigned int value = Serial.parseInt();
		if (value != 0) {
			config.clearAutoHoldInterval = value;
			EEPROM.put(0, config);
			EEPROM.get(0, config);
			Serial.print("clearAutoHold: ");
			Serial.println(config.clearAutoHoldInterval);
		}
	}
		break;
	default:
		// Statement(s)
		break; // Wird nicht ben�tigt, wenn Statement(s) vorhanden sind
	}

}

// The loop function is called in an endless loop
void loop() {

	currentMillis = millis();


	if (Serial.available()) {
		command = Serial.read();

		if (checkParity(command)) {
			if ((command & B00011000) == B00011000) {
				handleRequest();
			} else {
				Serial.println("OK");
				shutter = command & B00000111;
				newState = command & B00011000;
				newState = newState >> 3;
				state[shutter] = newState;
				stateLastChanged[shutter] = currentMillis;
			}

		}

	}



	// update buttons
	for (int i = 0; i < 16; i++) {
		// Update the Bounce instance :
		buttons[i].update();
	}

	// update state
	for (int i = 0; i < 8; i++) {

		// If UP-Button is pressed
		if (buttons[2 * i].fell() && buttons[2 * i + 1].read() == HIGH) {
			// If Up-Button was pressed right now, save the timestamp
			if (state[i] != UP) {
				buttonLastPressed[i] = currentMillis;
				state[i] = UP;
				stateLastChanged[i] = currentMillis;
			} else {
				state[i] = OFF;

			}

			// If DOWN-Button is pressed
		} else if (buttons[2 * i].read() == HIGH && buttons[2 * i + 1].fell()) {
			if (state[i] != DOWN) {
				buttonLastPressed[i] = currentMillis;
				state[i] = DOWN;
				stateLastChanged[i] = currentMillis;
			} else {
				state[i] = OFF;
			}

		} else if (buttons[2 * i].rose() || buttons[2 * i + 1].rose()) {
			// IF both buttons were released right now, check if the pressing interval was below threshold for "auto hold"

			if (state[i] == UP) {
				if (millis() - buttonLastPressed[i]
						> config.buttonHoldInterval) {
					state[i] = OFF;
				} else {
					state[i] = UP;
				}
			} else if (state[i] == DOWN) {
				if (millis() - buttonLastPressed[i]
						> config.buttonHoldInterval) {
					state[i] = OFF;
				} else {
					state[i] = DOWN;
				}
			}

		}
	}

	stateToRelay();

	if (currentMillis - lastAutoholdCheck > config.clearAutoHoldInterval / 2) {

		for (int i = 0; i < 8; i++) {
			if (state[i] != OFF
					&& (currentMillis - stateLastChanged[i]
							> config.clearAutoHoldInterval)) {
				state[i] = OFF;
			}
		}
		lastAutoholdCheck = millis();
	}

	delay(1);
}
