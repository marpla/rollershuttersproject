import serial


def addParity(byte):
    byte &= 0b00011111

    if not (byte & 0b00000001) ^ ((byte & 0b00000010) >> 1):
        byte |= 0b00100000
    if not ((byte & 0b00000100) >> 2) ^ ((byte & 0b00001000) >> 3):
        byte |= 0b01000000
    if not ((byte & 0b00010000) >> 4):
        byte |= 0b10000000
    return byte

def buildMessage(shutter, command):
    msg = shutter
    msg |= command << 3
    return addParity(msg)


ser = serial.Serial('/dev/ttyUSB1', timeout = 2000)  # open serial port


shutter = 2
command = 3


print(bin(buildMessage(2,3)))          

ser.write(bytes([buildMessage(0,3)]))
#ser.write(b'60001')
        

answer = ser.readline()
print(answer)