import socket
import time


def add_parity(byte):
    byte &= 0b00011111

    if not (byte & 0b00000001) ^ ((byte & 0b00000010) >> 1):
        byte |= 0b00100000
    if not ((byte & 0b00000100) >> 2) ^ ((byte & 0b00001000) >> 3):
        byte |= 0b01000000
    if not ((byte & 0b00010000) >> 4):
        byte |= 0b10000000
    return byte


def build_message(shutter, command):
    msg = shutter
    msg |= command << 3
    return add_parity(msg)


class RollerShutters:

    def __init__(self, host=None, port=None):
        self.connected = False
        self.host = host
        self.port = port
        if host is not None and port is not None:
            try:
                self.connect()
            except:
                raise

    def close(self):
        if self.connected:
            try:
                self.sock.close()
                self.connected = False
            except:
                raise

    def is_connected(self):
        return self.connected

    def connect(self):
        """
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            # Connect to server and send data

            self.sock.connect((self.host, self.port))
            self.sock.settimeout(2.0)

            self.connected = True
        except Exception as e:
            # print("error in connect", e)
            time.sleep(1)
            self.connect()

    def send_raw_message(self, raw_message):
        """ send raw message. sends raw_message as is """
        try:
            self.sock.sendall(raw_message)
            # print("sent message in send_raw_message")
        except Exception as e:
            # print("error in send_raw_message", e)
            time.sleep(1)
            self.connect()

    def move(self, shutter, cmd):
        try:
            self.send_raw_message(bytes([build_message(shutter, cmd)]))

            # if self.sock.recv(1024) == b'':
            #     self.connect()

        except:
            self.connect()

    def update_debounce(self, debounce):
        return self.update_config(3, debounce)

    def update_buttonhold(self, buttonhold):
        return self.update_config(4, buttonhold)

    def update_clearautohold(self, clearautohold):
        return self.update_config(5, clearautohold)

    def get_info(self):
        self.send_raw_message(bytes([build_message(2, 3)]))
        last_byte = b'0'
        answer = ""
        while True:
            last_byte = self.sock.recv(1)
            answer += last_byte.decode('ascii')
            if last_byte == b'\n':
                break
        return answer

    def update_config(self, cmd, value):
        self.send_raw_message(bytes([build_message(cmd, 3)]))

        self.send_raw_message(bytearray(str(value), encoding='ascii'))

        answer = ""
        while True:
            last_byte = self.sock.recv(1)
            answer += last_byte.decode('ascii')
            if last_byte == b'\n':
                break
        return answer

    def read_config(self, cmd):
        self.send_raw_message(bytes([build_message(cmd, 3)]))

        answer = ""
        while True:
            last_byte = self.sock.recv(1)
            answer += last_byte.decode('ascii')
            if last_byte == b'\n':
                break
        return answer

