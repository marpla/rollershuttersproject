from flask import Flask, render_template
from rollershutterstcp import RollerShutters

app = Flask(__name__)

shutters = RollerShutters('localhost', 7732)


@app.route('/')
def hello_world():
    return render_template('room1.html')

@app.route('/shutters/move/<int:shutter>/<int:cmd>')
def move_shutter(shutter, cmd):
    shutters.move(shutter,cmd)
    #print("moving")
    return ""

@app.route('/config')
def get_config():
    return shutters.get_info()




if __name__ == '__main__':
    app.run(host = '0.0.0.0')
